package com.a0bit.scorebash.interfaces;

import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.models.Player;

import java.util.ArrayList;

/**
 * Created by Alaina Traxler on 3/19/2018.
 */

public interface Callbacks {
    public void onObjectCallback(Object _object);
}
