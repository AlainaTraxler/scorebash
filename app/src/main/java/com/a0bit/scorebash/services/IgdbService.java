package com.a0bit.scorebash.services;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.interfaces.Callbacks;
import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.ui.AddGameActivity;
import com.android.volley.VolleyError;
import com.igdb.api_android_java.callback.onSuccessCallback;
import com.igdb.api_android_java.model.APIWrapper;
import com.igdb.api_android_java.model.Parameters;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class IgdbService {
    private Context mParent;
    private Dialog mDialog;
    private APIWrapper wrapper;

    //Searches for games and then updates the relevant activity. Updates occur within this function for async protection.
    public void searchForGame(String _searchString, Context _context){
        mParent = _context;

        Toast.makeText(mParent, "Searching for " + _searchString, Toast.LENGTH_SHORT).show();

        APIWrapper wrapper = new APIWrapper(mParent, Constants.KEY_IGDB);
        Parameters params = new Parameters()
                .addSearch(_searchString)
                .addExpand("platforms")
                .addLimit("20")
                .addFields("name");


        wrapper.search(APIWrapper.Endpoint.GAMES, params, new onSuccessCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                ((AddGameActivity) mParent).mGames.clear();
                for(int i = 0; i<result.length(); i++){
                    try{
                        JSONObject obj = result.getJSONObject(i);

                        ((AddGameActivity) mParent).mGames.add(processGameResult(obj));
                    }catch (JSONException e){
                        Toast.makeText(mParent, "Unable to process result", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

                ((AddGameActivity) mParent).mGameListAdapter.notifyDataSetChanged();

                Log.v(">> ", result.toString());
            }

            @Override
            public void onError(VolleyError error) {
                // Do something on error
                Toast.makeText(mParent, "Search failed", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
    }

    public void fetchGame(String _id, final Callbacks _callback){

        APIWrapper wrapper = new APIWrapper(mParent, Constants.KEY_IGDB);
        Parameters params = new Parameters()
                .addIds(_id)
                .addExpand("publishers,developers");

        wrapper.games(params, new onSuccessCallback() {
            @Override
            public void onSuccess(JSONArray jsonArray) {
                Game game = new Game();
                try {
                    game = processGameResult(jsonArray.getJSONObject(0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.v("In fetchGame for ", game.getName());
                _callback.onObjectCallback(game);
            }

            @Override
            public void onError(VolleyError volleyError) {
                Toast.makeText(mParent, "Unable to fetch game", Toast.LENGTH_SHORT).show();
                //TODO: implement null return?
            }
        });
    }

    public Game processGameResult(JSONObject _obj) throws JSONException {
        Game game = new Game();

        //TODO: modularize Field check and process

        if(_obj.has("name")){
//            Log.v("Found name!", _obj.getString("name"));
            game.setName(_obj.getString("name"));
        }

        if(_obj.has("id")){
            Log.v("Found id! ", _obj.getString("id"));
            game.setGameId(_obj.getString("id"));
        }

        //TODO: Modularize array check and processing

        if(_obj.has("platforms")){
            JSONArray array = _obj.getJSONArray("platforms");

            //Because non-expanded platform lists are integers, not objects
            if(!(array.get(0) instanceof Integer)){
                for(int i = 0; i < array.length(); i++){
                    if(array.getJSONObject(i).has("name")){
                        game.addPlatform(array.getJSONObject(i).getString("name"));
                    }
                }
            }
        }

        if(_obj.has("developers")){
            JSONArray array = _obj.getJSONArray("developers");

            //Because non-expanded developer lists are integers, not objects
            if(!(array.get(0) instanceof Integer)){
                for(int i = 0; i < array.length(); i++){
                    if(array.getJSONObject(i).has("name")){
                        game.addDeveloper(array.getJSONObject(i).getString("name"));
                    }
                }
            }
        }

        if(_obj.has("publishers")){
            JSONArray array = _obj.getJSONArray("publishers");

            //Because non-expanded publisher lists are integers, not objects
            if(!(array.get(0) instanceof Integer)){
                for(int i = 0; i < array.length(); i++){
                    if(array.getJSONObject(i).has("name")){
                        game.addPublisher(array.getJSONObject(i).getString("name"));
                    }
                }
            }
        }

        if(_obj.has("first_release_date")){
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date dateFormat = new Date(_obj.getLong("first_release_date"));
            game.setFirstReleaseDate(sdf.format(dateFormat));
        }

        if(_obj.has("cover")){
            game.setImageUrl("http://images.igdb.com/igdb/image/upload/t_cover_big/" + _obj.getJSONObject("cover").getString("cloudinary_id") + ".jpg");
        }

        if(_obj.has("summary")){
            game.setSummary(_obj.getString("summary"));
        }

        return game;
    }

    public void executeDialog(Game game){
        final Dialog dialog = new Dialog(mParent);
        dialog.setContentView(R.layout.dialog_game);

        ImageView imageView_BoxArt = (ImageView)dialog.findViewById(R.id.imageView_BoxArt);
        TextView textView_Overview = (TextView)dialog.findViewById(R.id.textView_Overview);
        TextView textView_DeveloperDetail = (TextView)dialog.findViewById(R.id.textView_DeveloperDetail);
        TextView textView_PublisherDetail = (TextView)dialog.findViewById(R.id.textView_PublisherDetail);
        TextView textView_ReleaseDateDetail = (TextView)dialog.findViewById(R.id.textView_ReleaseDateDetail);

        Picasso.get()
                .load(game.getImageUrl())
                .into(imageView_BoxArt);

        textView_Overview.setText(game.getSummary());
        textView_PublisherDetail.setText(game.getPublisherList());
        textView_DeveloperDetail.setText(game.getDeveloperList());
        textView_ReleaseDateDetail.setText(game.getFirstReleaseDate());

        dialog.show();
    }
}
