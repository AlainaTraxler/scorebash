package com.a0bit.scorebash.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class Score implements Serializable {
    public String pushId;
    public String playerId;
    public String playerInitials = "???";
    public String gameId;
    public String gameName;
    public List<String> criteria = new ArrayList<>();
    public List<String> platforms = new ArrayList<String>();
    public Long score = 0L;
    public String scoreType;

    public Score() {
    }

    public Score(String pushId, String playerId, String gameId, ArrayList<String> platforms, Long score, String _scoreType) {
        this.pushId = pushId;
        this.playerId = playerId;
        this.gameId = gameId;
        this.platforms = platforms;
        this.score = score;
        scoreType = _scoreType;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<String> criteria) {
        this.criteria = criteria;
    }

    public String getScoreType() {
        return scoreType;
    }

    public void setScoreType(String scoreType) {
        this.scoreType = scoreType;
    }

    public List getPlatforms() {
        return platforms;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

    public String getPlayerInitials() {
        return playerInitials;
    }

    public void setPlayerInitials(String playerInitials) {
        this.playerInitials = playerInitials;
    }
}
