package com.a0bit.scorebash.models;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class Player implements Serializable{
    public String pushId;
    public String email;
    public String initials = "???";
    public Long totalScore = 0L;
    public int totalCrowns = 0;
    public HashMap<String, String> scores = new HashMap<String, String>();
    public HashMap<String, String> rivals = new HashMap<String,String>();

    public Player() {
    }

    public Player(String pushId, String email, String initials) {
        this.pushId = pushId;
        this.email = email;
        this.initials = initials;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Long totalScore) {
        this.totalScore = totalScore;
    }

    public int getTotalCrowns() {
        return totalCrowns;
    }

    public void setTotalCrowns(int totalCrowns) {
        this.totalCrowns = totalCrowns;
    }

    public HashMap<String, String> getRivals() {
        return rivals;
    }

    public void setRivals(HashMap<String, String> rivals) {
        this.rivals = rivals;
    }

    public HashMap<String, String> getScores() {
        return scores;
    }

    public void setScores(HashMap<String, String> scores) {
        this.scores = scores;
    }
}
