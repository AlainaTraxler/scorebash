package com.a0bit.scorebash.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class Game implements Serializable {
    public String name = "???";
    public String gameId = "???";
    public List<String> platforms = new ArrayList<String>();
    public List<String> publishers = new ArrayList<String>();
    public List<String> developers = new ArrayList<String>();
    public String firstReleaseDate = "???";
    public String imageUrl;
    public String summary = "No summary available.";

    public Game(){
    }

    public Game(String name, String gameId) {
        this.name = name;
        this.gameId = gameId;
    }

    public Game(String name, String gameId, List<String> platforms) {
        this.name = name;
        this.gameId = gameId;
        this.platforms = platforms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<String> platforms) {
        this.platforms = platforms;
    }

    public List<String> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<String> publishers) {
        this.publishers = publishers;
    }

    public List<String> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<String> developers) {
        this.developers = developers;
    }

    public String getFirstReleaseDate() {
        return firstReleaseDate;
    }

    public void setFirstReleaseDate(String firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void addPlatform(String _platform){
        platforms.add(_platform);
    }

    public void addDeveloper(String _developer){
        developers.add(_developer);
    }

    public void addPublisher(String _publisher){
        publishers.add(_publisher);
    }

    public String getPlatformList(){
        String platformList = "";

        for(String s: platforms){
            platformList += s + " / ";
        }

        if(!platformList.equals("")){
            platformList = platformList.substring(0, platformList.length() - 3);
        }else{
            platformList = "???";
        }

        return platformList;
    }

    public String getPublisherList(){
        String publisherList = "";

        for(String s: publishers){
            publisherList += s + " / ";
        }

        if(!publisherList.equals("")){
            publisherList = publisherList.substring(0, publisherList.length() - 3);
        }else{
            publisherList = "???";
        }

        return publisherList;
    }

    public String getDeveloperList(){
        String developerList = "";

        for(String s: developers){
            developerList += s + " / ";
        }

        if(!developerList.equals("")){
            developerList = developerList.substring(0, developerList.length() - 3);
        }else{
            developerList = "???";
        }

        return developerList;
    }
}
