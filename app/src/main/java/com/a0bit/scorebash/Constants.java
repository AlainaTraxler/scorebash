package com.a0bit.scorebash;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class Constants {
    static public final String DB_PLAYERS = "players";
    static public final String DB_SCORES = "scores";
    static public final String DB_RIVALS = "rivals";

    public static final String KEY_IGDB = "3b85c0e2f92354a896dd51c4a5f17e9d";
}
