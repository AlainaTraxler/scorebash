package com.a0bit.scorebash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.a0bit.scorebash.interfaces.Callbacks;
import com.a0bit.scorebash.models.Player;
import com.a0bit.scorebash.ui.LoginActivity;
import com.a0bit.scorebash.ui.MenuActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

public class BaseActivity extends AppCompatActivity {
    public String TAG;
    public Context mParent;
    public Player mPlayer = null;

    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    public SharedPreferences mSharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mAuth = FirebaseAuth.getInstance();
        mParent = this;
        TAG = ">> " + mParent.getClass().getSimpleName();
        mSharedPrefs = getPreferences(Context.MODE_PRIVATE);

        setUpAuthListener();
        fetchUserProfile();
    }

    public void fetchUserProfile(){

        if(!mSharedPrefs.contains("PROFILE")){

            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
            Log.v(TAG, "Fetching profile");

            dbRef.child(Constants.DB_PLAYERS).child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mPlayer = dataSnapshot.getValue(Player.class);

                    Gson gson = new Gson();
                    SharedPreferences.Editor editor = mSharedPrefs.edit();
                    editor.putString("PROFILE", gson.toJson(mPlayer));
                    editor.commit();

                    Log.v(TAG, mPlayer.getInitials() + " stored in shared preferences");
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            mPlayer = new Gson().fromJson(mSharedPrefs.getString("PROFILE", ""), Player.class);
            Log.v(TAG, mPlayer.getInitials() + " found in shared preferences");
        }
    }

    private void setUpAuthListener(){
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // Player is signed in
                    if(mParent.getClass().getSimpleName().equals("LoginActivity")){
                        startActivity(new Intent(mParent, MenuActivity.class));
                    }
                } else {
                    if(!mParent.getClass().getSimpleName().equals("LoginActivity")){
                        startActivity(new Intent(mParent, LoginActivity.class));
                    }
                    // Player is signed out
                }
                // ...
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void logout(){
        Log.v(TAG, "in logout method");
        mAuth.signOut();
    }
}
