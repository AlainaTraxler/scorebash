package com.a0bit.scorebash.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.a0bit.scorebash.R;
import com.a0bit.scorebash.models.Score;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaina Traxler on 4/2/2018.
 */

public class ScoreListAdapter extends RecyclerView.Adapter<ScoreListAdapter.ScoreViewHolder> {
    public List<Score> mScores = new ArrayList<Score>();
    public Context mParent;


    public ScoreListAdapter(Context _parent, List<Score> _scores){
        mParent = _parent;
        mScores = _scores;
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_score, parent, false);
        ScoreViewHolder viewHolder = new ScoreViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        holder.bindScore(mScores.get(position));
    }

    @Override
    public int getItemCount() {
        return mScores.size();
    }

    public class ScoreViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_Score) TextView mTextView_Score;
        @BindView(R.id.textView_Initials) TextView mTextView_Initials;

        public Score mScore;

        public ScoreViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindScore(Score _score){
            mScore = _score;

            mTextView_Score.setText(mScore.getScore().toString());
            mTextView_Initials.setText(mScore.getPlayerInitials());
        }
    }
}
