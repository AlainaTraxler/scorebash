package com.a0bit.scorebash.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.models.Player;
import com.a0bit.scorebash.ui.ViewRivalsActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaina Traxler on 4/2/2018.
 */

//TODO: hide imageview dynamically

public class RivalListAdapter extends RecyclerView.Adapter<RivalListAdapter.RivalViewHolder> {
    public List<Player> mRivals = new ArrayList<Player>();
    public Context mParent;
    public String mCurrentUserId;

    public RivalListAdapter(List<Player> _rivals, Context _parent, String _currentUserId){
        mRivals = _rivals;
        mParent = _parent;
        mCurrentUserId = _currentUserId;
    }

    @Override
    public RivalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rival, parent, false);
        RivalViewHolder viewHolder = new RivalViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RivalViewHolder holder, int position) {
        holder.bindRival(mRivals.get(position));
    }

    @Override
    public int getItemCount() {
        return mRivals.size();
    }

    public class RivalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.textView_RivalInitials) TextView mTextView_RivalInitials;
        @BindView(R.id.textView_RivalEmail) TextView mTextView_RivalEmail;
        @BindView(R.id.imageView_AddRival) ImageView mImageView_AddRival;

        Player mPlayer = new Player();

        public RivalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mImageView_AddRival.setOnClickListener(this);
        }

        public void bindRival(Player player){
            mTextView_RivalInitials.setText(player.getInitials());
            mTextView_RivalEmail.setText(player.getEmail());

            mPlayer = player;
        }

        public void writeRivalToDatabase(){
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(Constants.DB_PLAYERS);

            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put(mPlayer.getPushId(),mPlayer.getInitials());

            dbRef.child(mCurrentUserId).child(Constants.DB_RIVALS).setValue(hash);

            Player rival = new Player();
            rival.setInitials(mPlayer.getInitials());
            rival.setPushId(mPlayer.getPushId());

            ((ViewRivalsActivity) mParent).mCurrentRivals.add(rival);
            ((ViewRivalsActivity) mParent).mViewRivalsListAdapter.notifyDataSetChanged();
            ((ViewRivalsActivity) mParent).mDialog.dismiss();
        }

        @Override
        public void onClick(View view) {
            if(view == mImageView_AddRival){
                writeRivalToDatabase();
            }
        }

    }
}
