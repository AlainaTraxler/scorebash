package com.a0bit.scorebash.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.a0bit.scorebash.R;
import com.a0bit.scorebash.interfaces.Callbacks;
import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.ui.AddGameActivity;
import com.a0bit.scorebash.ui.ViewScoresActivity;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaina Traxler on 3/15/2018.
 */

public class GameListAdapter extends RecyclerView.Adapter<GameListAdapter.GameViewHolder> {
    List<Game> mGames = new ArrayList<>();
    Context mParent;

    public GameListAdapter(List<Game> games, Context _parent){
        mGames = games;
        mParent = _parent;
//            this.mLocation = _location;
    }

    @Override
    public GameListAdapter.GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_game, parent, false);
        GameViewHolder viewHolder = new GameViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GameListAdapter.GameViewHolder holder, int position) {
        holder.bindGame(mGames.get(position));
    }

    @Override
    public int getItemCount() {
        return mGames.size();
    }

    public class GameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.textView_Name) TextView mTextView_Name;
        @BindView(R.id.imageView_AddGame) ImageView mImageView_AddGame;
        @BindView(R.id.textView_PlatformList) TextView mTextView_PlatformList;

        private View mItemView;

        public GameViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mItemView = itemView;

            itemView.setOnClickListener(this);
            mImageView_AddGame.setOnClickListener(this);

        }

        public void bindGame(Game game){
            mTextView_Name.setText(game.getName());

            mTextView_PlatformList.setText(game.getPlatformList());
        }

        @Override
        public void onClick(final View view) {
            Game game = mGames.get(getAdapterPosition());

            if(view == mImageView_AddGame){
                Bundle bundle = new Bundle();
                Log.v(">>", game.getPlatformList());
                bundle.putSerializable("GAME", game);

                mParent.startActivity(new Intent(mParent, ViewScoresActivity.class).putExtras(bundle));
            }else if(view == mItemView){
                ((AddGameActivity) mParent).api.fetchGame(mGames.get(getAdapterPosition()).getGameId(), new Callbacks() {
                    @Override
                    public void onObjectCallback(Object _object) {
                        ((AddGameActivity) mParent).api.executeDialog((Game) _object);
                    }
                });
            }
        }
    }
}
