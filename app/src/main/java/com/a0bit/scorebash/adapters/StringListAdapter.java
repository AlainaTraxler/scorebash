package com.a0bit.scorebash.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.a0bit.scorebash.R;
import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.ui.ViewScoresActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alaina Traxler on 3/21/2018.
 */

public class StringListAdapter extends RecyclerView.Adapter<StringListAdapter.StringViewHolder>{
    List<String> mStringList = new ArrayList<String>();
    Context mParent;
    String mListType;
    Dialog mDialog;
    public List<Boolean> mIsChecked = new ArrayList<Boolean>();

    public StringListAdapter(Context _parent, List<String> _stringList, String _listType, Dialog _dialog){
        mParent = _parent;
        mStringList = _stringList;
        mListType = _listType;
        mDialog = _dialog;
    }

    @Override
    public StringViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(mListType.equals("criteria")){
            view = LayoutInflater.from(mParent).inflate(R.layout.list_item_criteria, parent, false);
        }else{
            view = LayoutInflater.from(mParent).inflate(R.layout.list_item_platform, parent, false);
        }

        StringViewHolder viewHolder = new StringViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StringViewHolder holder, int position) {
        holder.bindString(mStringList.get(position));
    }

    @Override
    public int getItemCount() {
        return mStringList.size();
    }

    public List<String> harvestCheckedItems(){
        List<String> listOfStrings = new ArrayList<String>();

        for(int i = 0; i < mIsChecked.size(); i++){
            if(mIsChecked.get(i)){
                listOfStrings.add(mStringList.get(i));
            }
        }

        return listOfStrings;
    }

    public class StringViewHolder extends RecyclerView.ViewHolder {
        public CheckBox mCheckBox;
        private View mItemView;

        public StringViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;

            if(mListType.equals("criteria")){
                mCheckBox = (CheckBox) mItemView.findViewById(R.id.checkBox_Criteria);
            }else{
                mCheckBox = (CheckBox) mItemView.findViewById(R.id.checkBox_Platform);
            }

            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(mCheckBox.isChecked()){
                        mIsChecked.set(getAdapterPosition(), true);
                    }else{
                        mIsChecked.set(getAdapterPosition(), false);
                    }

                    Log.v(">> ", mIsChecked.toString());
                }
            });
        }

        public void bindString(String _string){
            mCheckBox.setText(_string);
            mIsChecked.add(false);
        }
    }

//    public List<String> harvestCheckBoxes(){
//        if(mListType.equals("criteria")){
//
//        }
//    }
}
