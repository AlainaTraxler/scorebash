package com.a0bit.scorebash.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.adapters.ScoreListAdapter;
import com.a0bit.scorebash.adapters.StringListAdapter;
import com.a0bit.scorebash.interfaces.Callbacks;
import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.models.Player;
import com.a0bit.scorebash.models.Score;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewScoresActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.floatingActionButton_AddGame) FloatingActionButton mFAB_AddGame;
    @BindView(R.id.recyclerView_ViewScores) RecyclerView mRecyclerView_ViewScores;

    public List<Score> mScores = new ArrayList<Score>();

    public Dialog mDialog;
    public Game mSelectedGame;

    public EditText mEditText_Score;
    public RecyclerView mRecyclerView_Criteria;
    public RecyclerView mRecyclerView_Platforms;
    public StringListAdapter mCriteriaAdapter;
    public StringListAdapter mPlatformAdapter;
    public ScoreListAdapter mScoreListAdapter;
    public Button mButton_SubmitScore;

    public List<String> mPlatformList = new ArrayList<String>();
    public List<String> mCriteriaList = new ArrayList<String>();

    public EditText mEditText_CriteriaInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_scores);
        ButterKnife.bind(this);

        if(this.getIntent().hasExtra("GAME")){
            mSelectedGame = (Game) this.getIntent().getSerializableExtra("GAME");
            addScore();
        }

        setUpScoreAdapter();
        fetchScores();

        mFAB_AddGame.setOnClickListener(this);
    }

    public void fetchScores(){
        Log.v(">>", "in fetchScores");

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(Constants.DB_PLAYERS);

        dbRef.child(mAuth.getCurrentUser().getUid()).child(Constants.DB_SCORES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot s:dataSnapshot.getChildren()){
                    Score score = s.getValue(Score.class);
                    mScores.add(score);

                    Log.v(">>>", score.getScore().toString());

                    mScoreListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == mFAB_AddGame){
            startActivity(new Intent(this, AddGameActivity.class));
        }else if(view == mButton_SubmitScore){
            Score score = harvestScore();

            if(score != null){
                writeScoreToDatabase(score);
            }
        }
    }

    public void writeScoreToDatabase(Score _score){
        //TODO Catch database write fails
        
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference ref = db.getReference(Constants.DB_PLAYERS).child(_score.getPlayerId()).child(Constants.DB_SCORES);

        String pushId = ref.push().getKey();
        _score.setPushId(pushId);

        ref.child(_score.getPushId()).setValue(_score);

        Toast.makeText(mParent, "Score submitted", Toast.LENGTH_SHORT).show();
        mDialog.dismiss();
    }

    public Score harvestScore(){
        Score score = new Score();

        //TODO Add other score types
        score.setScoreType("highScore");

        score.setGameId(mSelectedGame.getGameId());
        score.setGameName(mSelectedGame.getName());
        score.setPlayerId(mAuth.getCurrentUser().getUid());
        score.setCriteria(mCriteriaAdapter.harvestCheckedItems());
        score.setPlatforms(mPlatformAdapter.harvestCheckedItems());
        score.setPlayerInitials(mPlayer.getInitials());

        String scoreString = mEditText_Score.getText().toString();
        if(scoreString.length() > 0 && scoreString != null){
            score.setScore(Long.parseLong(scoreString));
        }else {
            score.setScore(null);
        }

        if(score.getCriteria().size() == 0){
            Toast.makeText(mParent, "Please select at least one criteria", Toast.LENGTH_SHORT).show();
            return null;
        }else if(score.getPlatforms().size() == 0){
            Toast.makeText(mParent, "Please select at least one platform", Toast.LENGTH_SHORT).show();
            return null;
        }else if(score.getScore() == null){
            Toast.makeText(mParent, "Please enter a valid score", Toast.LENGTH_SHORT).show();
            return null;
        }else{
            return score;
        }
    }

    //Executes a dialog for adding the score
    public void addScore(){
        mDialog = new Dialog(mParent);
        mDialog.setContentView(R.layout.dialog_add_score);

        mEditText_Score = (EditText) mDialog.findViewById(R.id.editText_ScoreInput);
        mRecyclerView_Criteria = (RecyclerView) mDialog.findViewById(R.id.recyclerView_Criteria);
        mEditText_CriteriaInput = (EditText) mDialog.findViewById(R.id.editText_CriteriaInput); 
        mRecyclerView_Platforms = (RecyclerView) mDialog.findViewById(R.id.recyclerView_Platforms);
        mButton_SubmitScore = (Button) mDialog.findViewById(R.id.button_SubmitScore);

        mPlatformList = mSelectedGame.getPlatforms();

        mEditText_CriteriaInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if(mEditText_CriteriaInput.getText().toString().trim().length() != 0){
                        Log.v(TAG, mEditText_CriteriaInput.getText().toString().trim());
                        mCriteriaList.add(mEditText_CriteriaInput.getText().toString().trim());
                        mCriteriaAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(mParent, "No criteria entered", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        setUpCriteriaAdapter(mSelectedGame);
        setUpPlatformAdapter();

        mButton_SubmitScore.setOnClickListener(this);

        mDialog.show();
    }

    public void setUpCriteriaAdapter(Game _game){
        //TODO: Add harvesting of pre-existing conditions
        mCriteriaList.add("1cc / defaults settings / no cheats");
        mCriteriaList.add("Wild");

        mCriteriaAdapter = new StringListAdapter(mParent, mCriteriaList, "criteria", mDialog);
        mRecyclerView_Criteria.setHasFixedSize(true);
        mRecyclerView_Criteria.setAdapter(mCriteriaAdapter);
        mRecyclerView_Criteria.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpPlatformAdapter(){
        mPlatformAdapter = new StringListAdapter(mParent, mPlatformList, "platform", mDialog);
        mRecyclerView_Platforms.setHasFixedSize(true);
        mRecyclerView_Platforms.setAdapter(mPlatformAdapter);
        mRecyclerView_Platforms.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpScoreAdapter(){
        Log.v(">>", "in setUpScoreAdapter");

        mScoreListAdapter = new ScoreListAdapter(mParent, mScores);
        mRecyclerView_ViewScores.setHasFixedSize(true);
        mRecyclerView_ViewScores.setAdapter(mScoreListAdapter);
        mRecyclerView_ViewScores.setLayoutManager(new LinearLayoutManager(this));
    }
}
