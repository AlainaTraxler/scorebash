package com.a0bit.scorebash.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.models.Player;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuActivity extends BaseActivity implements View.OnClickListener{
    @BindView(R.id.floatingActionButton_Rivals) FloatingActionButton mFAB_Rivals;
    @BindView(R.id.floatingActionButton_Scores) FloatingActionButton mFAB_Scores;
    @BindView(R.id.floatingActionButton_Settings) FloatingActionButton mFAB_Settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        Log.v(TAG, mAuth.getCurrentUser().getEmail());
        setUpButtons();
    }

    public void fetchAndStoreProfile(){

    }

    @Override
    public void onClick(View view) {
        if(view == mFAB_Rivals){
            startActivity(new Intent(this, ViewRivalsActivity.class));
        }else if(view == mFAB_Scores){
            startActivity(new Intent(this, ViewScoresActivity.class));

        }else if(view == mFAB_Settings){
            startActivity(new Intent(this, SettingsActivity.class));
        }
    }

    private void setUpButtons(){
        mFAB_Rivals.setOnClickListener(this);
        mFAB_Scores.setOnClickListener(this);
        mFAB_Settings.setOnClickListener(this);
    }
}
