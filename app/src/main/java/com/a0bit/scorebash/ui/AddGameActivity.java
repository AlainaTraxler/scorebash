package com.a0bit.scorebash.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SearchView;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.adapters.GameListAdapter;
import com.a0bit.scorebash.models.Game;
import com.a0bit.scorebash.services.IgdbService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddGameActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.searchView_FindGame) SearchView mSearchView_FindGame;
    @BindView(R.id.recyclerView_FoundGames) RecyclerView mRecyclerView_FoundGames;

    public IgdbService api = new IgdbService();

    public List<Game> mGames= new ArrayList<Game>();
    public GameListAdapter mGameListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_game);
        ButterKnife.bind(this);

        setUpRecyclerView();
        setUpSearchView();
    }

    @Override
    public void onClick(View view) {

    }

    private void setUpSearchView(){
        mSearchView_FindGame.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                api.searchForGame(s, mParent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    public void setUpRecyclerView(){
        mGameListAdapter = new GameListAdapter(mGames, mParent);
        mRecyclerView_FoundGames.setHasFixedSize(true);
        mRecyclerView_FoundGames.setAdapter(mGameListAdapter);
        mRecyclerView_FoundGames.setLayoutManager(new LinearLayoutManager(this));
    }

    public void addGame(Game _game){

    }

    public void updateRecyclerView(){

    }

}
