package com.a0bit.scorebash.ui;

import android.app.Dialog;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.adapters.RivalListAdapter;
import com.a0bit.scorebash.models.Player;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewRivalsActivity extends BaseActivity implements View.OnClickListener{
    @BindView(R.id.floatingActionButton_AddRival) FloatingActionButton mFAB_AddRival;
    @BindView(R.id.recyclerView_ViewRivals) RecyclerView mRecyclerView_ViewRivals;

    public SearchView mSearchView_SearchForFival;
    public RecyclerView mRecyclerView_FoundRivals;

    public RivalListAdapter mFoundRivalListAdapter;
    public List<Player> mFoundRivals = new ArrayList<Player>();

    public RivalListAdapter mViewRivalsListAdapter;
    public List<Player> mCurrentRivals = new ArrayList<Player>();

    public Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_rivals);
        ButterKnife.bind(this);

        setUpViewRivalsRecyclerView();

        mFAB_AddRival.setOnClickListener(this);
    }

    public void executeAddRivalDialog(){
        mDialog = new Dialog(mParent);
        mDialog.setContentView(R.layout.dialog_add_rival);

        mSearchView_SearchForFival = (SearchView) mDialog.findViewById(R.id.searchView_SearchForRival);
        mRecyclerView_FoundRivals = (RecyclerView) mDialog.findViewById(R.id.recyclerView_FoundRivals);

        setUpFoundRivalsRecyclerView();
        fetchCurrentRivals();
        setUpSearchView();

        mDialog.show();
    }

    private void setUpSearchView(){
        mSearchView_SearchForFival.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mFoundRivals.clear();
                mFoundRivalListAdapter.notifyDataSetChanged();
                searchDatabaseForRivals(s.toLowerCase().trim());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    public void searchDatabaseForRivals(final String rivalEmail){
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(Constants.DB_PLAYERS);

        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot s:dataSnapshot.getChildren()){
                    Player player = s.getValue(Player.class);
                    if(player.getEmail().contains(rivalEmail) && !player.getPushId().equals(mAuth.getCurrentUser().getUid())){
                        mFoundRivals.add(player);
                        mFoundRivalListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == mFAB_AddRival){
            executeAddRivalDialog();
        }
    }

    public void setUpFoundRivalsRecyclerView(){
        mFoundRivalListAdapter = new RivalListAdapter(mFoundRivals, mParent, mAuth.getCurrentUser().getUid());
        mRecyclerView_FoundRivals.setHasFixedSize(true);
        mRecyclerView_FoundRivals.setAdapter(mFoundRivalListAdapter);
        mRecyclerView_FoundRivals.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpViewRivalsRecyclerView(){
        mViewRivalsListAdapter = new RivalListAdapter(mCurrentRivals, mParent, mAuth.getCurrentUser().getUid());
        mRecyclerView_ViewRivals.setHasFixedSize(true);
        mRecyclerView_ViewRivals.setAdapter(mViewRivalsListAdapter);
        mRecyclerView_ViewRivals.setLayoutManager(new LinearLayoutManager(this));
    }

    public void fetchCurrentRivals(){
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference(Constants.DB_PLAYERS);
        Log.v(">>", "In fetchCurrentRivals");

        dbRef.child(mAuth.getCurrentUser().getUid()).child(Constants.DB_RIVALS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v(">>", "In onDataChange");
                for(DataSnapshot s:dataSnapshot.getChildren()){
                    Player rival = new Player();
                    rival.setPushId(s.getKey().toString());
                    rival.setInitials(s.getValue().toString());

                    Log.v(">>",rival.getInitials());

                    mCurrentRivals.add(rival);
                    mViewRivalsListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
