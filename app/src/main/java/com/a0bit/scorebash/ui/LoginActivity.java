package com.a0bit.scorebash.ui;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.Constants;
import com.a0bit.scorebash.R;
import com.a0bit.scorebash.models.Player;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener{
    @BindView(R.id.editText_Email) EditText mEditText_Email;
    @BindView(R.id.editText_Password) EditText mEditText_Password;
    @BindView(R.id.button_LogIn) Button mButton_LogIn;
    @BindView(R.id.button_SignUp) Button mButton_SignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mButton_LogIn.setOnClickListener(this);
        mButton_SignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String email = mEditText_Email.getText().toString();
        String password = mEditText_Password.getText().toString();

        if(view == mButton_LogIn){
            signIn(email, password);
        }else if(view == mButton_SignUp){
            createAccount(email, password);
        }
    }

    private void createAccount(final String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(mParent, "Welcome to ScoreBash!", Toast.LENGTH_SHORT).show();
                            // Sign in success, update UI with the signed-in user's information
                            createPlayerInDatabase(email);
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(mParent, "Unable to create account",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signIn(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(mParent, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void createPlayerInDatabase(String _email){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(Constants.DB_PLAYERS);
        String initials = "???";
        String uId = mAuth.getCurrentUser().getUid();

        //Chops email down for initials
        if(!_email.substring(0,3).contains("@")){
            initials = _email.substring(0,3).toUpperCase();
        }else{
            initials = _email.substring(0,_email.indexOf("@")).toUpperCase();
        }

        Player player = new Player(uId, _email, initials);

        ref.child(uId).setValue(player);
    }
}
