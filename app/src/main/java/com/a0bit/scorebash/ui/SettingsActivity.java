package com.a0bit.scorebash.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.a0bit.scorebash.BaseActivity;
import com.a0bit.scorebash.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.textView_LogOut) TextView mTextView_LogOut;
    @BindView(R.id.textView_Initials) TextView mTextView_Initials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);


    }

    @Override
    public void onClick(View view) {
        if(view == mTextView_Initials){

        }else if(view == mTextView_LogOut){
            logout();
        }
    }

    private void setUpButtons(){
        mTextView_Initials.setOnClickListener(this);
        mTextView_LogOut.setOnClickListener(this);
    }
}
