# README #


### What is this repository for? ###

* Know how modern games have those lovely leaderboards that let you see what your friends scored? Know how older games don't have that? ScoreBash is an app to create social leaderboards between you and your friendly rivals, for any game you can think of. At long last, you can see who the true Gyruss champion is.
* Version: alpha

### How do I get set up? ###

* At the moment, this is just a blind download and compile targetting API 21+.

### Who do I talk to? ###

* eloavox@gmail.com